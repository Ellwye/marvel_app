import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Personnage } from './personnage';
import { Md5  } from 'ts-md5/dist/md5';

@Injectable({
  providedIn: 'root'
})
export class PersonnageService {
  private md5 = new Md5();
  private privateKey : string = '16ec3c23d41fd79006971b5331715198169fd4a4';
  private publicKey : string = '28ed4d70902b1d74adabca6cd57fc69e';
  private ts : number = Date.now();
  private hash : any = this.md5.appendStr(this.ts+this.privateKey+this.publicKey).end();
  private personnages_url : string = "http://gateway.marvel.com/v1/public/characters?orderBy=name&ts=" + this.ts + "&apikey=28ed4d70902b1d74adabca6cd57fc69e&hash=" + this.hash;

  constructor(private http: HttpClient) { }


  public getPersonnages(): Observable<any> {
    return this.http.get(this.personnages_url);
  }

  getPersonnage(id: number): Observable<any> {
    const url = `http://gateway.marvel.com/v1/public/characters/${id}?&ts=${this.ts}&apikey=28ed4d70902b1d74adabca6cd57fc69e&hash=${this.hash}`;
    return this.http.get(url);
  }

}
