import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListePersonnagesComponent } from './liste-personnages/liste-personnages.component';
import { DetailsPersonnagesComponent } from './details-personnages/details-personnages.component';


const routes: Routes = [
  { path: 'liste-personnages', component: ListePersonnagesComponent },
  { path: 'details-personnages/:id', component: DetailsPersonnagesComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
