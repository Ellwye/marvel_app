import { Component, OnInit } from '@angular/core';
import { PersonnageService } from '../personnage.service';

@Component({
  selector: 'app-liste-personnages',
  templateUrl: './liste-personnages.component.html',
  styleUrls: ['./liste-personnages.component.scss']
})
export class ListePersonnagesComponent implements OnInit {
  public personnages = [];

  constructor(private personnageService: PersonnageService) {
    personnageService.getPersonnages().subscribe(personnageListe => {
      console.log(personnageListe.data.results);
      for(let personnage of personnageListe.data.results){
        this.personnages.push(personnage)
      }
    })
    console.log("Personnages :", this.personnages);
  }

  ngOnInit(): void { }

}
