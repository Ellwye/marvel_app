import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule }    from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListePersonnagesComponent } from './liste-personnages/liste-personnages.component';
import { DetailsPersonnagesComponent } from './details-personnages/details-personnages.component';

@NgModule({
  declarations: [
    AppComponent,
    ListePersonnagesComponent,
    DetailsPersonnagesComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
