import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PersonnageService } from '../personnage.service';

@Component({
  selector: 'app-details-personnages',
  templateUrl: './details-personnages.component.html',
  styleUrls: ['./details-personnages.component.scss']
})
export class DetailsPersonnagesComponent implements OnInit {
  public personnage = [];
  private id = parseInt(this.route.snapshot.paramMap.get('id'));

  constructor(private route: ActivatedRoute, private router: Router, private personnageService: PersonnageService) { 

    personnageService.getPersonnage(this.id).subscribe(personnageDetails => {
      for(let detail of personnageDetails.data.results){
        this.personnage.push(detail);
      }
    })
    console.log("Personnage details :", this.personnage);

  }

  ngOnInit(): void { }

}
