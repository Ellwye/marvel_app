import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsPersonnagesComponent } from './details-personnages.component';

describe('DetailsPersonnagesComponent', () => {
  let component: DetailsPersonnagesComponent;
  let fixture: ComponentFixture<DetailsPersonnagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailsPersonnagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsPersonnagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
